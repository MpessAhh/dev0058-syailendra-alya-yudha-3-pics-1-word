3 Pics 1 Word - Trivia Game
=====================

Contoh game: [3 Pics 1 Word (FB Instant Games)](https://www.facebook.com/instantgames)

Catatan Pengerjaan
------------------

Buat game web menggunakan Phaser dengan game diatas sebagai acuan. Sebelum mulai mengerjakan lakukan tahapan-tahapan seperti berikut:

1.  Mainkan game dan tulis daftar Scene & Fitur yang ada  
    Tahap pertama mainkan game sambil tulis semua daftar scene, dialog, fitur yang ada pada game tersebut. Daftar ini akan menjadi acuan kamu membuat flowchart dan mengerjakan project nantinya. Pastikan kamu coba setiap fitur yang ada & pahami logika / mekaniknya
2.  Buat Flowchart  
    Kemudian buat flowchart jalannya game dari daftar yang sudah kamu cari, commit ke BitBucket apabila sudah selesai
3.  Buat Checklist  
    Apabila flowchart telah disetujui, buat daftar checklist todo yang harus kamu kerjakan pada project dan perkirakan berapa lama waktu pengerjaan untuk masing-masing todo tersebut. Contoh pada list todo kamu tambahkan durasinya dalam satuan hari / jam:  
    *   Scene Menu (5 jam)
        *   Layout UI
        *   Animasi UI
    *   SceneInfo (2 jam)
        *   dst...
4.  Kumpulkan aset  
    Selanjutnya cari aset-aset yang diperlukan dalam game, mulai dari gambar, musik, sound effect dst
5.  Build Game  
    Baru kemudian kerjakan project sesuai flowchart dan daftar yang sudah kamu buat
6.  Test & fixing  
      
    

### Fitur

*   Fitur-fitur yang tidak berkaitan langsung pada game seperti **More, Ads** dan fitur2 sosial seperti **Rank, Share** tidak perlu diimplementasikan secara coding, cukup kamu buat Layout nya saja  
      
    

### **GAME**

*   **Perlu diperhatikan:** setiap ada daftar object selalu dibuat dinamis -> contoh daftar karakter, pet, dst
*   **Fokus selesaikan CORE GAME dulu**, kalau sudah fix & tidak ada bug baru kerjakan fitur-fitur lain
*   Struktur folder:  
    
        /FolderProject
        	/assets
        		/fonts -> berisi font (jika ada)
        		/gfx -> berisi gambar jpg/png
        		/sfx -> berisi musik & sound effect
        	/js
        	/json
        	flowchart.png
        	index.html
    

* * *

Laporan
-------

*   Commit project setiap hari ke BitBucket yang sudah di share
*   Buat laporan di web gamelab 2x (11.30 dan 16.30). Laporkan apa yang dikerjakan pada waktu tersebut selengkap-lengkapnya. Contoh  
    
        Mengerjakan SceneMenu
        - Layout UI 0-100%
        - Callback 0-100%
        - Animasi UI 0-20%
        
        Next todo:
        - Animasi UI 20-100%
        - Mulai mengerjakan SceneBermain?