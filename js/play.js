// Membuat scene baru
var ScenePlay = new Phaser .Class({
	Extends: Phaser.Scene,

	initialize:
	//============================================================INIT============================================================//
	// Constructor
	function ScenePlay ()
	{
		// Pembautan scene baru
		Phaser.Scene.call(this, { key: 'sceneplay' });
	},

	preload: function ()
	{
		this.load.image('B_BACK' , 'assets/gfx/B_BACK.png');
		this.load.image('B_NEXT' , 'assets/gfx/B_NEXT.png');
		this.load.image('B_PLAY' , 'assets/gfx/B_PLAY.png');
		this.load.image('B_SOUND' , 'assets/gfx/B_SOUND.png');
		this.load.image('BG' , 'assets/gfx/BG.png');
		this.load.image('BIG1' , 'assets/gfx/BIG1.png');
		this.load.image('BIG2' , 'assets/gfx/BIG2.png');
		this.load.image('BIG3' , 'assets/gfx/BIG3.png');
		this.load.image('BIG4' , 'assets/gfx/BIG4.png');
		this.load.image('COLD1' , 'assets/gfx/COLD1.png');
		this.load.image('COLD2' , 'assets/gfx/COLD2.png');
		this.load.image('COLD3' , 'assets/gfx/COLD3.png');
		this.load.image('COLD4' , 'assets/gfx/COLD4.png');
		this.load.image('DRY1' , 'assets/gfx/DRY1.png');
		this.load.image('DRY2' , 'assets/gfx/DRY2.png');
		this.load.image('DRY3' , 'assets/gfx/DRY3.png');
		this.load.image('DRY4' , 'assets/gfx/DRY4.png');
		this.load.image('OLD1' , 'assets/gfx/OLD1.png');
		this.load.image('OLD2' , 'assets/gfx/OLD2.png');
		this.load.image('OLD3' , 'assets/gfx/OLD3.png');
		this.load.image('OLD4' , 'assets/gfx/OLD4.png');
		this.load.image('PLUS' , 'assets/gfx/PLUS.png');
		this.load.image('RANK' , 'assets/gfx/RANK.png');
		this.load.image('TAMBAH' , 'assets/gfx/TAMBAH.png');
		this.load.image('WET1' , 'assets/gfx/WET1.png');
		this.load.image('WET2' , 'assets/gfx/WET2.png');
		this.load.image('WET3' , 'assets/gfx/WET3.png');
		this.load.image('WET4' , 'assets/gfx/WET4.png');

		this.clicking = false;

		this.isGameRunning = false;
	},
	//============================================================#INIT============================================================//





	//============================================================CUSTOM FUNCTION============================================================//
	startGame: function()
	{
		this.tweens.add({
			targets: this.B_SOUND,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

		this.tweens.add({
			targets: this.WET1,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

		this.tweens.add({
			targets: this.WET2,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

		this.tweens.add({
			targets: this.WET3,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

		this.tweens.add({
			targets: this.B_PLAY,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

		this.tweens.add({
			targets: this.RANK,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

		this.tweens.add({
			targets: this.PLUS,
			ease: 'Back.easeIn',
			duration: 100,
			scaleX: 0,
			scaleY: 0
		});

			this.isGameRunning = true;
	},
	//============================================================#	CUSTOM FUNCTION============================================================//





	//============================================================EVENT LISTENER============================================================//
	startInputEvents: function()
	{
		this.input.on('pointerup', this.onPointerUp, this);

		this.input.on('gameobjectdown', this.onObjectClick, this);
		this.input.on('gameobjectup', this.onObjectClickEnd, this);
		this.input.on('gameobjectover', this.onObjectOver, this);
		this.input.on('gameobjectout', this.onObjectOut, this);
	},

	onObjectClick: function (pointer, gameObject)
	{
		if (!this.isGameRunning && gameObject == this.B_PLAY)
		{
			this.B_PLAY.setTint(0x616161);
			this.clicking = true;
		}
	},

	onObjectOver: function (pointer, gameObject)
	{
		if (!this.clicking) return;
		if (!this.isGameRunning && gameObject == this.B_PLAY)
		{
			this.B_PLAY.setTint(0x616161);
		}
	},

	onObjectOut: function (pointer, gameObject)
	{
		if (!this.clickicking) return;
		if (!this.isGameRunning && gameObject == this.B_PLAY)
		{
			this.B_PLAY.setTint(0xffffff);
		}
	},

	onObjectClickEnd: function (pointer, gameObject)
	{
		if (!this.isGameRunning && gameObject == this.B_PLAY)
		{
			this.B_PLAY.setTint(0xffffff);
			this.startGame();
		}
	},

	onPointerUp: function(pointer, currentlyOver)
	{
		console.log("MouseUp");
		this.clicking = false;
	},
	//============================================================#EVENT LISTENER============================================================//





	//============================================================MAIN============================================================//
	create: function ()
	{
		this.add.image(768/2,1366/2,'BG');
		this.add.image(100,100,'B_SOUND');
		this.add.image(250,300, 'WET1');
		this.add.image(550,300, 'WET2');
		this.add.image(768/2, 600,'WET3');
		this.add.image(768/2, 850, 'B_PLAY');
		this.B_PLAY.setInteractive();
		this.add.image(600, 1100, 'RANK');
		this.add.image(600, 1200, 'PLUS');

		this.time.delayedCall(0, this.startInputEvents, [], this);

	}
	//============================================================#MAIN============================================================//
});

// Membuat konfigurasi scene
var config = {
	type: Phaser.AUTO,
	width: 768,
	height: 1366,
	scene: [ScenePlay]
};

// Membuat & Menjalankan Objek game sesuai konfigurasi yang telah dibuat (secara otomatis dibuat oleh library Phaser)
var game = new Phaser.Game(config);